﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public struct InfoOfVehicleInMovement
{
    public Vehicle Vehicle;
    public float TimeToMove;
    public Consumer Consumer;
    public Producer Producer;

    public override bool Equals(object objectToCompare)
    {
        var itemToCompare = (InfoOfVehicleInMovement) objectToCompare;
        return GetType() == itemToCompare.GetType() && itemToCompare.Consumer == Consumer &&
               itemToCompare.Producer == Producer &&
               itemToCompare.Vehicle == Vehicle;
    }
}

/*
 * Generate a RoundRobinProducer with a Queue of all the vehicles that just has been sent to a selected Producer
 * Organize all the RoundRobinProducers 
 * 
 */
public class RoundRobinSchedulerProducer
{
    private Producer _producer;
    private Coroutine _movementCoroutine, _timerCoroutine, _timerQueueStoppedCoroutine;
    private int roundRobinsCreated = 0;
    public Producer Producer => _producer;
    public RoundRobinProducer CurrentRoundRobinProducer => _lastRoundRobinProducer;

    private List<RoundRobinProducer> _roundRobinProducers = new List<RoundRobinProducer>();
    private RoundRobinProducer _lastRoundRobinProducer;

    public void Initialize(Producer producer)
    {
        _producer = producer;
        Random.InitState(Random.Range(0, 100));
    }


    public void InstantiateNewVehicles(List<Consumer> consumersWithVehiclesToInstantiate)
    {
        Random.InitState((int) DateTime.Now.Ticks);

        var roundRobinProducer = new RoundRobinProducer();
        _lastRoundRobinProducer = roundRobinProducer;

        var timeToMove = Random.Range(5, 10);
        roundRobinProducer.Initialize(_producer, timeToMove, roundRobinsCreated);

        Debug.Log($"Created new Round Robin {roundRobinsCreated}");
        roundRobinsCreated++;
        roundRobinProducer.OnEmptyQueue += EmptyQueue;

        try
        {
            foreach (var consumer in consumersWithVehiclesToInstantiate)
            {
                var vehicleInstance = consumer.GetVehicleInstance();

                if (!vehicleInstance) continue;
                var vehicle = vehicleInstance.GetComponent<Vehicle>();
                if (_roundRobinProducers.Any(x => x.Queue.Any(queue => queue.Vehicle == vehicle)))
                {
                    Debug.Log("FAIL");
                }

                roundRobinProducer.EnqueueVehicle(vehicle, consumer);
            }

            _roundRobinProducers.Add(roundRobinProducer);
        }
        catch (Exception e)
        {
            Debug.Log($"One consumer hasn't any vehicles to use {e}");
        }
    }

    private void EmptyQueue(RoundRobinProducer roundRobinProducer)
    {
        Debug.Log($"Remove Round Robin Producer {roundRobinProducer}");
        roundRobinProducer.OnEmptyQueue -= EmptyQueue;
        _roundRobinProducers.Remove(roundRobinProducer);
    }
}