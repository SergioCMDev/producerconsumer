﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Entity which moves between Consumers and Producers.
 * Blue means that is going to Producer, Black that is paused and yellow that is coming back to Consumer
 */
public class Vehicle : TileEntity
{
    [SerializeField] private float _distanceToCheckIfNear = 0.11f;
    private Consumer _consumer;
    private Producer _producer;

    private List<GridTile> _pathToFollow;
    private Coroutine _moveCoroutine;

    private bool _isReturningToConsumer,
        _hasStartedMovement,
        _initialized,
        _hasProduct,
        _paused,
        _useCollisionsToCheck = false,
        _reachedProducerButNoItem;

    private Color _originalColour;

    private int _pathFindingIndex,
        _timeToWaitIfSuspended = 2,
        _timeToWaitIfProducerIsLocked = 6,
        _timeToWaitUntilNewMovement = 2;

    public event Action<InfoOfVehicleInMovement> OnVehicleIsInProducer = delegate { };
    public event Action<InfoOfVehicleInMovement> OnSuspendedDueToEmptyProducer = delegate { };

    public Consumer Consumer
    {
        get => _consumer;
        set => _consumer = value;
    }

    public Producer Producer
    {
        get => _producer;
        set => _producer = value;
    }

    public bool IsReturningToConsumer => _isReturningToConsumer;
    public bool IsPaused => _paused;
    public bool Initialized => _initialized;


    private void Awake()
    {
        _originalColour = GetOriginalColor();
    }

    public void PrepareValues(Consumer consumer, Producer producer)
    {
        if (producer == null || consumer == null)
        {
            Debug.LogError("Producer and Consumer must be filled");
        }

        _producer = producer;
        _consumer = consumer;
        var path = consumer.GetPathToProducer(producer);
        _pathToFollow = new List<GridTile>(path);
        _initialized = true;
    }

    public void Initialize()
    {
        _isReturningToConsumer = false;
    }

    private void MoveToConsumer()
    {
        ShowSprite();
        _hasStartedMovement = true;
        StartDefaultMovement();
    }

    public void MoveToProducer()
    {
        if (_pathToFollow == null || _pathToFollow.Count == 0) return;
        _hasStartedMovement = true;

        StartDefaultMovement();
    }

    private void StartDefaultMovement()
    {
        transform.position = _pathToFollow[0].tuplePosition;
        _pathFindingIndex = 1;
        _moveCoroutine = StartCoroutine(MoveAlongPath(_pathToFollow));
    }

    public bool SamePositionAsProducer()
    {
        try
        {
            return transform.position == _producer.transform.position;
        }
        catch (Exception e)
        {
            Debug.LogError($"Vehicle {name} has Producer null {e}");
        }

        return false;
    }

    private bool SamePositionAsConsumer()
    {
        try
        {
            return transform.position == _consumer.transform.position;
        }
        catch (Exception e)
        {
            Debug.LogError($"Vehicle {name} has Consumer null {e}");
        }

        return false;
    }

    private bool IsAwayFromDestination(GridTile nextPosition)
    {
        return Vector3.Distance(transform.position, nextPosition.tuplePosition) > _distanceToCheckIfNear;
    }

    private IEnumerator MoveAlongPath(IReadOnlyList<GridTile> pathToFollow)
    {
        Debug.Log($"Start Movement {gameObject.name} {_pathFindingIndex}");
        if (IsReturningToConsumer)
        {
            ChangeSpriteToComingColour();
        }

        while (_pathFindingIndex < pathToFollow.Count)
        {
            Debug.Log($"Move {gameObject.name} to {_pathFindingIndex}");

            var nextPosition = pathToFollow[_pathFindingIndex];

            while (IsAwayFromDestination(nextPosition))
            {
                while (_paused)
                {
                    yield return null;
                }

                transform.position =
                    Vector3.MoveTowards(transform.position, nextPosition.tuplePosition, 2 * Time.deltaTime);

                yield return null;
            }

            transform.position = nextPosition.tuplePosition;

            if (CanReleaseProducer())
            {
                _producer.ReleaseProducer();
            }

            _pathFindingIndex++;

            if (SamePositionAsConsumer() || SamePositionAsProducer()) break;
            yield return new WaitForSeconds(_timeToWaitUntilNewMovement);
        }

        if (_useCollisionsToCheck) yield break;
        CheckPositionAfterEndingFollowPath();
    }

    public void CheckPositionAfterEndingFollowPath()
    {
        if (SamePositionAsProducer())
        {
            VehicleReachToProducer();
        }
        else if (SamePositionAsConsumer())
        {
            Debug.Log($"Coming to Consumer {gameObject.name}");

            VehicleReachToConsumer();
        }
    }

/*
 * In case there is no item or vehicle must be suspended, It moves one position back and suspends the vehicle
 */
    private IEnumerator GoBack()
    {
        var nextPosition = new GridTile();
        try
        {
            _pathFindingIndex = _pathToFollow.Count - 2;
            if (_pathFindingIndex < 0) yield break;
            nextPosition = _pathToFollow[_pathFindingIndex];
        }
        catch (Exception e)
        {
            Debug.LogError($"Error While going back {name} Exception {e}");
        }

        Debug.Log($"We have position To Move {gameObject.name}");
        while (IsAwayFromDestination(nextPosition))
        {
            transform.position =
                Vector3.MoveTowards(transform.position, nextPosition.tuplePosition, 2 * Time.deltaTime);
            yield return null;
        }

        Debug.Log("End movement backwards Move");

        OnSuspendedDueToEmptyProducer.Invoke(new InfoOfVehicleInMovement()
        {
            Consumer = _consumer,
            Producer = _producer,
            TimeToMove = -1,
            Vehicle = this
        });
    }


    private void VehicleReachToConsumer()
    {
        if (!_hasProduct) return;

        _consumer.AddProduct();
        _consumer.RemoveVehicle(_producer, this);
        _pathToFollow.Reverse();
        ChangeSpriteToOriginalColour();
        gameObject.SetActive(false);
        Clean();
        StopCoroutine(_moveCoroutine);
    }

    private bool CanReleaseProducer()
    {
        return _producer.Occupied && _isReturningToConsumer && !SamePositionAsProducer() &&
               _producer.IsOccupiedByThisVehicle(this);
    }

    //Can use Collisions to check if we are in Consumers or Producers but is a bit messy
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_useCollisionsToCheck) return;

        var producer = other.gameObject.GetComponent<Producer>();
        var consumer = other.gameObject.GetComponent<Consumer>();

        if (producer)
        {
            VehicleReachToProducer();
        }

        if (consumer)
        {
            VehicleReachToConsumer();
        }
    }

    private void VehicleReachToProducer()
    {
        Debug.Log($"Coming to Producer {gameObject.name}");
        if (CanGetProductOfProducer(_producer) && !_paused)
        {
            Debug.Log($"Pick item {gameObject.name} ");
            PrepareToReturnToConsumer(_producer);
        }
        else
        {
            Debug.Log($"Suspend {gameObject.name}");
            _reachedProducerButNoItem = true;
            StartCoroutine(GoBack());
        }
    }

    private void PrepareToReturnToConsumer(Producer producer)
    {
        Debug.Log($"Prepare to come back to Costumer {gameObject.name}");
        HideSprite();
        StopCoroutine(_moveCoroutine);
        _hasProduct = true;
        _paused = false;
        _pathToFollow.Reverse();
        _isReturningToConsumer = true;
        _reachedProducerButNoItem = false;
        producer.LockProducer(this);
        producer.GetProduct();
        ChangeSpriteToComingColour();
        OnVehicleIsInProducer.Invoke(new InfoOfVehicleInMovement()
        {
            Consumer = _consumer,
            Vehicle = this,
            Producer = _producer,
        });
        MoveToConsumer();
    }

    private void Clean()
    {
        _consumer = null;
        _isReturningToConsumer = false;
        _hasStartedMovement = false;
        _producer = null;
        _paused = false;
        _hasProduct = false;
        _pathFindingIndex = 0;
        _initialized = false;
        _reachedProducerButNoItem = false;
    }

    private void ChangeSpriteToOriginalColour()
    {
        ChangeColour(_originalColour);
    }

    private void ChangeSpriteToSuspendedColour()
    {
        ChangeColour(Color.black);
    }

    private void ChangeSpriteToComingColour()
    {
        ChangeColour(Color.yellow);
    }

    private bool CanGetProductOfProducer(Producer producer)
    {
        return !_hasProduct && Producer == producer && producer.HasProducts() && !_producer.Occupied;
    }

    public bool HasMovedBefore()
    {
        return _hasStartedMovement;
    }

    public void SuspendMovement()
    {
        if (!HasMovedBefore() || _isReturningToConsumer)
        {
            return;
        }

        ChangeSpriteToSuspendedColour();
        Debug.Log(
            $"Suspend vehicle {gameObject.name} at {_pathFindingIndex} it has {_pathToFollow.Count} movements more");
        _paused = true;
    }

    public void ResumeMovement()
    {
        Debug.Log($"Resume Movement of vehicle {gameObject.name} at {_pathFindingIndex}");
        if (!IsReturningToConsumer)
        {
            ChangeSpriteToOriginalColour();
        }

        _paused = false;
        if (_reachedProducerButNoItem && _producer.HasProducts())
        {
            _moveCoroutine = StartCoroutine(MoveAlongPath(_pathToFollow));
        }
    }
}