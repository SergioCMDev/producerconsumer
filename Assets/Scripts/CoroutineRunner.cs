﻿using UnityEngine;

public class CoroutineRunner : MonoBehaviour
{
    public static CoroutineRunner instance;

    private void Awake()
    {
        if(instance) return;
        instance = this;
    }
}