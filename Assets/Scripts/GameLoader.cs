﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/*
 *
 * Perform the game initialization, it will call Building Loader to instantiate Entities as well the Grid Creator to create the Grid.
 * We can set the number of Producers and Consumers that will be activated at start in the Editor
 * 
 */
public class GameLoader : MonoBehaviour
{
    [SerializeField] private GameObject _gridCreatorPrefab, _buildingLoaderPrefab, _pathfindingPrefab;
    [SerializeField] private CanvasPresenter _canvasPresenter;
    [SerializeField] private int _producersToCreate, _consumersToCreate;
    private Astar _astar;
    private GridCreator _gridCreator;
    private BuildingLoader _buildingLoader;

    void Awake()
    {
        _gridCreator = Instantiate(_gridCreatorPrefab, transform).GetComponent<GridCreator>();
        _buildingLoader = Instantiate(_buildingLoaderPrefab, transform).GetComponent<BuildingLoader>();
        _astar = Instantiate(_pathfindingPrefab, transform).GetComponent<Astar>();
    }

    private void Start()
    {
        if (_producersToCreate == 0 && _consumersToCreate == 0)
            Debug.LogError("Must Create at least one producer and one consumer");

        _gridCreator.GenerateGrid();

        _buildingLoader.Initialize();
        _buildingLoader.SetGrid(_gridCreator.GetGrid());
        _astar.SetGrid(_gridCreator.GetGrid());
        _buildingLoader.SetPathfinding(_astar);

        List<GameObject> consumerInstances = new List<GameObject>();
        List<GameObject> producersInstances = new List<GameObject>();
        List<Vector2> positionsAdded = new List<Vector2>();

        Random.InitState(Random.Range(0, 100));

        GenerateInstancesConsumers(positionsAdded, consumerInstances);
        GenerateInstancesProducers(positionsAdded, producersInstances);

        _canvasPresenter.SetData(consumerInstances, producersInstances);
        //In case we need to, we can create buildings by hand
        // var consumerInstance1 = _buildingLoader.GenerateConsumerAtPosition(new Vector2(5, 3));
        // consumerInstances.Add(consumerInstance1);
        
        // var producerInstance1 = _buildingLoader.GenerateProducerAtPosition(new Vector2(3, 4));
        // producersInstances.Add(producerInstance1);

        _buildingLoader.GeneratePathfindingBetweenConsumersAndProducers(consumerInstances, producersInstances);
    }

    /*
     * Return random positions one time each one
     * Use a list to exclude the old positions
     */
    private Vector2 GetRandomPosition(List<Vector2> positionsAdded)
    {
        Vector2 positionToCreate;
        do
        {
            Debug.Log("Calculating random position");
            positionToCreate = CreateRandomPosition();
        } while (positionsAdded.Contains(positionToCreate));

        return positionToCreate;
    }

    private void GenerateInstancesConsumers(List<Vector2> positionsAdded, ICollection<GameObject> consumerInstances)
    {
        for (var i = 0; i < _consumersToCreate; i++)
        {
            var positionToCreate = GetRandomPosition(positionsAdded);

            positionsAdded.Add(positionToCreate);
            var consumerInstance = _buildingLoader.GenerateConsumerAtPosition(positionToCreate);
            consumerInstances.Add(consumerInstance);
        }
    }

    private void GenerateInstancesProducers(List<Vector2> positionsAdded, ICollection<GameObject> consumerInstances)
    {
        for (var i = 0; i < _producersToCreate; i++)
        {
            var positionToCreate = GetRandomPosition(positionsAdded);

            positionsAdded.Add(positionToCreate);
            var consumerInstance = _buildingLoader.GenerateProducerAtPosition(positionToCreate);
            consumerInstances.Add(consumerInstance);
        }
    }

    private Vector2 CreateRandomPosition()
    {
        Random.InitState((int) DateTime.Now.Ticks);
        int randomXValue = Random.Range(0, _gridCreator.HorizontalSize);
        int randomYValue = Random.Range(0, _gridCreator.VerticalSize);
        var positionToCreate = new Vector2(randomXValue, randomYValue);
        return positionToCreate;
    }
}