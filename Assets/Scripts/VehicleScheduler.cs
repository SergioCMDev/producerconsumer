﻿using System.Collections.Generic;
using System.Linq;


/*
 * Organizes all the RoundRobinProducers.
 * When a new item is produced it will find the RoundRobin attached to the producer and manage the sent vehicles.
 * Will search the consumers nearby the producer and creates a new vehicle when needed(or when it can create) and make the vehicles start the routes towards the producers.
 */

public class VehicleScheduler
{
    private List<Consumer> _consumersCreated = new List<Consumer>();
    private List<Producer> _producersCreated = new List<Producer>();

    private List<RoundRobinSchedulerProducer> _roundRobinSchedulerProducers = new List<RoundRobinSchedulerProducer>();
    private bool _activateNewVechicles = false;

    public void Initialize(List<Producer> producersCreated, List<Consumer> consumersCreated)
    {
        _consumersCreated = consumersCreated;
        _producersCreated = producersCreated;

        foreach (var producerInstance in producersCreated)
        {
            producerInstance.OnProducerHasProduced += ProducerHasProduced;
        }
    }

    private void ProducerHasProduced(Producer producer)
    {
        var consumersNearby = GetConsumersWithProducerNearby(producer);
        if (consumersNearby.Count == 0) return;
        var consumersToSendVehicles = new List<Consumer>();

        //In case this producer has 2 consumers nearby it will use Round Robin to schedule the vehicles,
        if (consumersNearby.Count > 1)
        {
            foreach (var consumer in consumersNearby)
            {
                consumersToSendVehicles.Add(consumer);
            }

            if (consumersToSendVehicles.Count == 0) consumersToSendVehicles = consumersNearby;
            StartRoundRobinLogic(producer, consumersToSendVehicles);
            return;
        }

        //If not, it will just sent a vehicle towards the producer
        foreach (var consumer in consumersNearby)
        {
            consumer.DoRouteToProducer(producer);
        }
    }

    private void StartRoundRobinLogic(Producer producer, List<Consumer> consumersToSendVehicles)
    {
        bool VehiclesInstantiatedEmpty(RoundRobinSchedulerProducer roundRobinSchedulerProducer)
        {
            return roundRobinSchedulerProducer.CurrentRoundRobinProducer.Queue.Count == 0;
        }

        RoundRobinSchedulerProducer robinSchedulerProducer;
        if (ExistsPreviousRoundRobinOfProducer(producer))
        {
            robinSchedulerProducer = _roundRobinSchedulerProducers.Single(x => x.Producer == producer);
            _roundRobinSchedulerProducers.Remove(robinSchedulerProducer);
        }
        else
        {
            robinSchedulerProducer = new RoundRobinSchedulerProducer();
            robinSchedulerProducer.Initialize(producer);
        }

        robinSchedulerProducer.InstantiateNewVehicles(consumersToSendVehicles);
        _roundRobinSchedulerProducers.Add(robinSchedulerProducer);

        if (VehiclesInstantiatedEmpty(robinSchedulerProducer)) return;
        robinSchedulerProducer.CurrentRoundRobinProducer.StartMovement();
    }

    private bool ExistsPreviousRoundRobinOfProducer(Producer producer)
    {
        return _roundRobinSchedulerProducers.Exists(x => x.Producer == producer);
    }

    private List<Consumer> GetConsumersWithProducerNearby(Producer producer)
    {
        var consumersNearby = new List<Consumer>();
        foreach (var consumerInstance in _consumersCreated)
        {
            if (consumerInstance.gameObject.activeSelf && consumerInstance.HasProducerNearby(producer))
            {
                consumersNearby.Add(consumerInstance);
            }
        }

        return consumersNearby;
    }

    public void Destroy()
    {
        foreach (var producerInstance in _producersCreated)
        {
            var producer = producerInstance.GetComponent<Producer>();
            producer.OnProducerHasProduced -= ProducerHasProduced;
        }
    }
}