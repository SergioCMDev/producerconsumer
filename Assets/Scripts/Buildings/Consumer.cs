﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct ProducerVehicleTuple
{
    public Producer Producer;
    public List<Vehicle> Vehicles;
}

public struct ProducerPath
{
    public Producer Producer;
    public List<GridTile> Path;
}

/*
 *	Entity which will consume items.
 *	Has a list with the vehicles sent to each producer
 * 	Manage the vehicles which has been sent, can remove and add vehicles to the lists (maybe part of the logic could be moved to VehicleScheduler)
 *	They are the red items in the scene
 */

public class Consumer : TileEntity
{
    private int _products;
    private List<GameObject> _instantiatedVehicles;
    private List<Producer> _producersNearby = new List<Producer>();
    private List<ProducerVehicleTuple> _producerVehicleTuples;
    private List<ProducerPath> _producerPaths = new List<ProducerPath>();

    public event Action<TileEntity, int> OnShowQuantity = delegate { };
    public event Action OnHideQuantity = delegate { };
    public void Initialize()
    {
        _producerVehicleTuples = new List<ProducerVehicleTuple>();
    }

    public void SetInstantiatedVehicles(List<GameObject> instantiatedVehicles)
    {
        _instantiatedVehicles = instantiatedVehicles;
    }

    private GameObject RetrieveInstantiatedVehicle()
    {
        Func<GameObject, bool> CanUseVehicle()
        {
            return vehicle =>
                !vehicle.activeInHierarchy &&
                !vehicle.GetComponent<Vehicle>()
                    .Initialized; // Check this because a Initialized vehicle is inside a queue ready to move, but we can't select it again
        }

        foreach (var vehicle in _instantiatedVehicles.Where(CanUseVehicle()))
        {
            return vehicle;
        }
        
        return null;
    }

    /*
     * Move a vehicle to the selected producer
     */
    public void DoRouteToProducer(Producer producer)
    {
        var vehicleInstance = RetrieveInstantiatedVehicle();
        Debug.Log($"Vehicle {vehicleInstance} inicia camino hacia producer {producer} desde Consumer {name}");
        DoRouteToProducerWithVehicle(producer, vehicleInstance);
    }

    /*
     * Move a certain vehicle to the selected producer
     */
    public void DoRouteToProducerWithVehicle(Producer producer, GameObject vehicleInstance)
    {
        if (!vehicleInstance) return;
        var vehicle = vehicleInstance.GetComponent<Vehicle>();
        if (vehicle.IsPaused && vehicle.HasMovedBefore())
        {
            vehicle.ResumeMovement();
            return;
        }

        Debug.Log($"Start Route with Vehicle {vehicleInstance.name} towards {producer}");
        vehicleInstance.gameObject.SetActive(true);
        if (!vehicle.Initialized)
        {
            vehicle.PrepareValues(this, producer);
            vehicle.Initialize();
        }

        vehicle.MoveToProducer();
        AddVehicleToSentVehicles(producer, vehicle);
    }

    private void AddVehicleToSentVehicles(Producer producer, Vehicle vehicle)
    {
        bool ProducerHasBeenAddedToList()
        {
            return _producerVehicleTuples.Exists(x => x.Producer == producer);
        }

        if (ProducerHasBeenAddedToList())
        {
            var producerVehicleTuple = _producerVehicleTuples.Single(x => x.Producer == producer);
            producerVehicleTuple.Vehicles.Add(vehicle);
        }
        else
        {
            var producerVehicleTuple = new ProducerVehicleTuple()
            {
                Producer = producer,
                Vehicles = new List<Vehicle> {vehicle}
            };
            _producerVehicleTuples.Add(producerVehicleTuple);
        }
    }

    public void RemoveVehicle(Producer producer, Vehicle vehicle)
    {
        bool ProducerHasBeenAddedToList()
        {
            return _producerVehicleTuples.Exists(x => x.Producer == producer);
        }

        if (!ProducerHasBeenAddedToList()) return;

        var producerVehicleTuple = _producerVehicleTuples.Single(x => x.Producer == producer);
        producerVehicleTuple.Vehicles.Remove(vehicle);
    }

    private void OnMouseEnter()
    {
        OnShowQuantity.Invoke(this, _products);
    }

    private void OnMouseExit()
    {
        OnHideQuantity.Invoke();
    }

    public void AddProduct()
    {
        _products++;
    }

    public void AddPathToProducer(Producer producer, List<GridTile> pathConsumerToProducer)
    {
        bool HasSavedBeforeProducer()
        {
            return _producerPaths.Exists(x => x.Producer == producer);
        }

        if (HasSavedBeforeProducer()) return;
        _producerPaths.Add(new ProducerPath()
        {
            Producer = producer,
            Path = pathConsumerToProducer
        });
        _producersNearby.Add(producer);
    }

    public List<GridTile> GetPathToProducer(Producer producer)
    {
        if (!_producerPaths.Exists(x => x.Producer == producer)) return new List<GridTile>();
        return _producerPaths.Single(x => x.Producer == producer).Path;
    }

    public bool HasProducerNearby(Producer producer)
    {
        var producerIsNearby = _producerPaths.Exists(x => x.Producer == producer);
        return producerIsNearby;
    }

    public GameObject GetVehicleInstance()
    {
        var vehicleInstance = RetrieveInstantiatedVehicle();

        return !vehicleInstance ? null : vehicleInstance;
    }
}