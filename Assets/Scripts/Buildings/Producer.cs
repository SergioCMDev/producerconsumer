﻿using System;
using UnityEngine;
using Utils;

/*
 *
 * Produce ítems,
 * They are the white items in the scene
 */
public class Producer : TileEntity
{
    [SerializeField] private float _timeToProduce;
    [SerializeField] private bool _canProduce;
    private int _producedProducts;
    private Timer _timer;
    private bool _occupied;
    private Vehicle _occupiedByVehicle;
    public event Action<Producer> OnProducerHasProduced = delegate { };
    public event Action<TileEntity, int> OnShowQuantity = delegate { };
    public event Action OnHideQuantity = delegate { };

    public bool Occupied => _occupied;

    private void Awake()
    {
        _timer = new Timer();
        _timer.TimeToWait(_timeToProduce);
    }

    private void Produce()
    {
        _timer.OnTimerEnds -= Produce;
        if (_canProduce)
        {
            _producedProducts++;
            OnProducerHasProduced.Invoke(this);
            Debug.Log($"New item  {gameObject.name}");
        }

        Initialize();
    }

    private void OnMouseEnter()
    {
        OnShowQuantity.Invoke(this, _producedProducts);
    }

    private void OnMouseExit()
    {
        OnHideQuantity.Invoke();
    }

    public void GetProduct()
    {
        Debug.Log($"Picked product of {gameObject.name}");
        _producedProducts--;
    }

    public void Initialize()
    {
        _timer.OnTimerEnds += Produce;
        StartCoroutine(_timer.TimerCoroutine());
    }

    public void ReleaseProducer()
    {
        _occupied = false;
    }

    public void LockProducer(Vehicle vehicle)
    {
        _occupied = true;
        _occupiedByVehicle = vehicle;
    }

    public bool HasProducts()
    {
        return _producedProducts > 0;
    }

    public bool IsOccupiedByThisVehicle(Vehicle vehicle)
    {
        return _occupiedByVehicle == vehicle;
    }
}