﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

/*
 *
 *	Implements a Round Robin algorithm with a FIFO Queue.
 *	Each producer has one for each set of vehicles sent to it , it will schedule Vehicles coming to it based on the queue.
 *	It will suspend vehicles when their time to move pass and allow the next in the queue to move
 */
public class RoundRobinProducer
{
    private Coroutine _movementCoroutine, _timerCoroutine; 
    private Producer _producer;
    private int _id;
    private Queue<InfoOfVehicleInMovement> _queue;
    private Timer _timer;
    private float _timeToMoveVehicleInQueue = 5; 
    private bool _pauseQueue, _initialized;
    private bool _queueIsRunning, _timerCoroutineStarted;
    public Queue<InfoOfVehicleInMovement> Queue => _queue;
    public event Action<RoundRobinProducer> OnEmptyQueue = delegate { };

    public void Initialize(Producer producer, float movementTime, int id)
    {
        _timeToMoveVehicleInQueue = movementTime;
        _timer = new Timer();
        _timer.TimeToWait(_timeToMoveVehicleInQueue);
        _queue = new Queue<InfoOfVehicleInMovement>();
        _initialized = true;
        _producer = producer;
    }

    public void StartMovement()
    {
        if (_queueIsRunning) return;
        _movementCoroutine = CoroutineRunner.instance.StartCoroutine(MoveVehiclesInQueue());
    }


    public void EnqueueVehicle(Vehicle vehicle, Consumer consumer)
    {
        vehicle.PrepareValues(consumer, _producer);
        var itemToEnqueue = new InfoOfVehicleInMovement()
        {
            Consumer = consumer,
            Producer = _producer,
            TimeToMove = _timeToMoveVehicleInQueue,
            Vehicle = vehicle
        };

        itemToEnqueue.Vehicle.OnVehicleIsInProducer += RemoveVehicleOfQueue;
        itemToEnqueue.Vehicle.OnSuspendedDueToEmptyProducer +=
            UpdateVehicleInMovementDueToToSuspensionInProducer;

        if (_queue.Contains(itemToEnqueue))
        {
            return;
        }

        Debug.Log($"Add new vehicle {itemToEnqueue.Vehicle.name} to Producer Round Robin {_id}");
        _queue.Enqueue(itemToEnqueue);
    }

    /*
     *
    * Use a circular Queue to move the vehicles which are going to a certain Producer
    * When the time of movement of a vehicles end we dequeue it and allow the next to move.
    * I use a few timers to control when the time is past as well as to check if the queue has a lock to unlock it
    * I also use events to allow the queue to know when a vehicle has come to the Producer to remove it from the queue (it must be tuple in first position)
    *    or when it's in the Producer but the producer hasn't any product to pick , in this case this vehicle will be suspended.
    * When a vehicle is sent I lock the queue to only allow it to be selected until any timer end
    */
    private IEnumerator MoveVehiclesInQueue()
    {
        _queueIsRunning = true;
        _timer = new Timer();

        while (_queue.Count > 0)
        {
            try
            {
                var tupleToExecute = _queue.Peek();
                var consumer = tupleToExecute.Consumer;

                _timer.TimeToWait(tupleToExecute.TimeToMove);
                _timer.OnTimerEnds += UpdateVehicleInMovement;

                _timerCoroutine = CoroutineRunner.instance.StartCoroutine(_timer.TimerCoroutine());
                _timerCoroutineStarted = true;

                consumer.DoRouteToProducerWithVehicle(tupleToExecute.Producer, tupleToExecute.Vehicle.gameObject);

                _pauseQueue = true;
            }
            catch (Exception e)
            {
                Debug.Log($"Exception when moving Queue of Producer {_producer.name} {e}");
            }

            while (_pauseQueue)
            {
                yield return null;
            }
        }

        _queueIsRunning = false;
        _timerCoroutineStarted = false;
        _timer.OnTimerEnds -= UpdateVehicleInMovement;
        _pauseQueue = false;

        StopCoroutine(_movementCoroutine);
        StopCoroutine(_timerCoroutine);

        _movementCoroutine = null;
        _timerCoroutine = null;

        OnEmptyQueue.Invoke(this);
    }
    
    private void UpdateVehicleInMovementDueToToSuspensionInProducer(InfoOfVehicleInMovement infoOfVehicle)
    {
        infoOfVehicle.Vehicle.OnSuspendedDueToEmptyProducer -= UpdateVehicleInMovementDueToToSuspensionInProducer;

        if (infoOfVehicle.Vehicle.SamePositionAsProducer())
        {
            Debug.Log("Vehicle Has stopped in Producer");

            // return;
        }

        UpdateVehicleInMovement();
    }

    /*
     * When a vehicle reach one Producer, the vehicle is removed from the queue as well as removed all the events attached
     */
    private void RemoveVehicleOfQueue(InfoOfVehicleInMovement infoOfVehicle)
    {
        Debug.Log($"Remove {infoOfVehicle.Vehicle.name}");

        _timerCoroutineStarted = false;
        _timer.OnTimerEnds -= UpdateVehicleInMovement;
        StopCoroutine(_timerCoroutine);

        infoOfVehicle.Vehicle.OnSuspendedDueToEmptyProducer -=
            UpdateVehicleInMovementDueToToSuspensionInProducer;
        infoOfVehicle.Vehicle.OnVehicleIsInProducer -= RemoveVehicleOfQueue;
        if (!_queue.Contains(infoOfVehicle))
        {
            Debug.Log("FAIL");
            return;
        }

        if (!_queue.Peek().Equals(infoOfVehicle))
        {
            do
            {
                var itemToEnqueueAgain = _queue.Dequeue();
                _queue.Enqueue(itemToEnqueueAgain);
            } while (_queue.Peek().Equals(infoOfVehicle));

            return;
        }

        _queue.Dequeue();
        _pauseQueue = false;
    }


    private void StopCoroutine(Coroutine coroutine)
    {
        CoroutineRunner.instance.StopCoroutine(coroutine);
    }

    /*
     * Update of the current head of the queue
     */
    private void UpdateVehicleInMovement()
    {
        try
        {
            _timer.OnTimerEnds -= UpdateVehicleInMovement;
            _timerCoroutineStarted = false;
            StopCoroutine(_timerCoroutine);

            if (_queue.Count == 0) return;

            if (_queue.Peek().Vehicle.SamePositionAsProducer())
            {
                Debug.Log("Vehicle Has stopped in Producer");
                _queue.Peek().Vehicle.CheckPositionAfterEndingFollowPath();

                return;
            }

            var itemToEnqueue = _queue.Dequeue();

            if (itemToEnqueue.Vehicle.IsReturningToConsumer)
            {
                Debug.Log("FAIL");
                return;
            }

            Debug.Log($"Update Vehicle {itemToEnqueue.Vehicle.name}");
            ChangeHeadOfTheQueue(itemToEnqueue);
        }
        catch (Exception e)
        {
            Debug.Log($"Exception when Updating vehicle {e}");
        }
    }

    private void CheckVehicleData(InfoOfVehicleInMovement itemToEnqueue)
    {
        if (itemToEnqueue.Vehicle.Consumer != null && itemToEnqueue.Vehicle.Producer != null) return;
        itemToEnqueue.Vehicle.Consumer = itemToEnqueue.Consumer;
        itemToEnqueue.Vehicle.Producer = itemToEnqueue.Producer;
    }

    private void ChangeHeadOfTheQueue(InfoOfVehicleInMovement itemToEnqueue)
    {
        CheckVehicleData(itemToEnqueue);
        itemToEnqueue.Vehicle.SuspendMovement();
        _queue.Enqueue(itemToEnqueue);
        _pauseQueue = false;
    }
}