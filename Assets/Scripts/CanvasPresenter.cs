﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CanvasPresenter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    private List<Consumer> _consumers = new List<Consumer>();
    private List<Producer> _producers = new List<Producer>();

    private void Start()
    {
        HideQuantity();
    }

    private void ShowQuantity(TileEntity entity, int quantity)
    {
        _text.gameObject.SetActive(true);
        _text.SetText($"Entity {entity} has {quantity}");
    }

    private void HideQuantity()
    {
        _text.gameObject.SetActive(false);
    }

    public void SetData(List<GameObject> consumerInstances, List<GameObject> producersInstances)
    {
        foreach (var consumerInstance in consumerInstances)
        {
            var consumer = consumerInstance.GetComponent<Consumer>();
            consumer.OnHideQuantity += HideQuantity;
            consumer.OnShowQuantity += ShowQuantity;
            _consumers.Add(consumer);
        }

        foreach (var producersInstance in producersInstances)
        {
            var producer = producersInstance.GetComponent<Producer>();
            producer.OnHideQuantity += HideQuantity;
            producer.OnShowQuantity += ShowQuantity;
            _producers.Add(producer);
        }
    }

    private void OnDestroy()
    {
        foreach (var consumer in _consumers)
        {
            consumer.OnHideQuantity -= HideQuantity;
            consumer.OnShowQuantity -= ShowQuantity;
        }

        foreach (var producer in _producers)
        {
            producer.OnHideQuantity -= HideQuantity;
            producer.OnShowQuantity -= ShowQuantity;
        }
    }
}