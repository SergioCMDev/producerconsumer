﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public struct MovementsQuantity
{
    public Vector2 AxisValues;
}

/*
 * Class to perform an A* search in the Grid, it allows the vehicles to move between Consumers and Producers and vice versa.
 */
public class Astar : MonoBehaviour
{
    [SerializeField] private List<MovementsQuantity> _movementsQuantity;
    [SerializeField] private float _horizontalCostAStar, _verticalCostAStar;
    
    private List<GridTile> _gridTuples;
    
    private List<GridTile> _processedTuples;
    private List<GridTile> _openTuples;
    
    private List<GridTile> _pathTuples;

    public void SetGrid(List<GridTile> gridTuples)
    {
        _gridTuples = gridTuples;

        foreach (var tuple in _gridTuples)
        {
            tuple.GCost = int.MaxValue;
            tuple.GetFCost();
            tuple.cameFromNode = null;
        }
    }

    public List<GridTile> GetPath(Vector2 originPosition, Vector2 destinationPosition)
    {
        _processedTuples = new List<GridTile>();
        _openTuples = new List<GridTile>();

        foreach (var tuple in _gridTuples)
        {
            tuple.GCost = int.MaxValue;
            tuple.GetFCost();
            tuple.cameFromNode = null;
        }
        
        var gridTupleOrigin = GetGridTupleByPosition(originPosition);
        var gridTupleDestination = GetGridTupleByPosition(destinationPosition);

        _openTuples.Add(gridTupleOrigin);

        SetTupleValues(gridTupleOrigin, 0, originPosition, destinationPosition);

        while (ExistsNodesToCheck())
        {
            var actualTuple = GetTupleWinMinimumFCost(_openTuples);
            if (PositionIsDestination(actualTuple, gridTupleDestination))
            {
                return CalculatePath(gridTupleDestination);
            }

            _openTuples.Remove(actualTuple);
            _processedTuples.Add(actualTuple);

            var neighbours = GetNeighbours(actualTuple);
            foreach (var neighbour in neighbours)
            {
                if (TilePositionIsCorrect(neighbour.tuplePosition) && IsWalkable(neighbour) 
                    || (PositionIsDestination(neighbour, gridTupleDestination)))
                {
                    if (_processedTuples.Contains(neighbour)) continue;

                    float tentativeGCost =
                        actualTuple.GCost + GetHeuristicToDestination(actualTuple.tuplePosition, neighbour.tuplePosition);

                    if (PossibleBestWay(tentativeGCost, neighbour))
                    {
                        neighbour.cameFromNode = actualTuple;
                        SetTupleValues(neighbour, tentativeGCost, neighbour.tuplePosition,
                            gridTupleDestination.tuplePosition);
                        if (!_openTuples.Contains(neighbour)) _openTuples.Add(neighbour);
                    }
                }
            }
        }

        //No path found
        return new List<GridTile>();
    }

    private static bool PossibleBestWay(float tentativeGCost, GridTile neighbour)
    {
        return tentativeGCost < neighbour.GCost;
    }

    private bool ExistsNodesToCheck()
    {
        return _openTuples.Count > 0;
    }

    private void SetTupleValues(GridTile gridTile, float gCost, Vector2 originPosition, Vector2 destinationPosition)
    {
        gridTile.GCost = gCost;
        gridTile.HValue = GetHeuristicToDestination(originPosition, destinationPosition);
        gridTile.GetFCost();
    }

    private List<GridTile> CalculatePath(GridTile gridTileDestination)
    {
        var pathTuples = new List<GridTile>();

        pathTuples.Add(gridTileDestination);
        var currentNode = gridTileDestination;

        GetNodesFromEndToStart(currentNode, pathTuples);

        pathTuples.Reverse();
        return pathTuples;
    }

    private static void GetNodesFromEndToStart(GridTile currentNode, List<GridTile> pathTuples)
    {
        while (StillHaveNodesToGoBack(currentNode))
        {
            var actualNode = currentNode.cameFromNode;
            pathTuples.Add(actualNode);
            currentNode = actualNode;
        }
    }

    private static bool StillHaveNodesToGoBack(GridTile currentNode)
    {
        return currentNode.cameFromNode != null;
    }

    private GridTile GetTupleWinMinimumFCost(List<GridTile> openList)
    {
        bool BetterNode(GridTile tuple, float oldFValue)
        {
            return tuple.FValue < oldFValue;
        }

        var bestTuple = openList[0];
        var minValue = openList[0].FValue;

        foreach (var tuple in openList)
        {
            if (BetterNode(tuple, minValue))
            {
                bestTuple = tuple;
            }
        }

        return bestTuple;
    }

    private List<GridTile> GetNeighbours(GridTile gridTileOrigin)
    {
        var neighbours = new List<GridTile>();
        foreach (var movement in _movementsQuantity)
        {
            var neighbourPosition = gridTileOrigin.tuplePosition + movement.AxisValues;
            var neighbourGridTuple = GetGridTupleByPosition(neighbourPosition);

            neighbours.Add(neighbourGridTuple);
        }

        return neighbours;
    }


    private bool IsWalkable(GridTile neighbourGridTile)
    {
        return neighbourGridTile.tileEntity.Walkable;
    }

    private bool TilePositionIsCorrect(Vector2 tuplePosition)
    {
        return tuplePosition.x >= 0 && tuplePosition.y >= 0
                                    && tuplePosition.x <= _gridTuples[_gridTuples.Count - 1].tuplePosition.x
                                    && tuplePosition.y <= _gridTuples[_gridTuples.Count - 1].tuplePosition.y;
    }

    private float GetHeuristicToDestination(Vector2 originPosition, Vector2 destinationPosition)
    {
        var distanceX = Math.Abs(destinationPosition.x - originPosition.x);
        var distanceY = Math.Abs(destinationPosition.y - originPosition.y);

        var heuristic = distanceX * _horizontalCostAStar + distanceY * _verticalCostAStar;
        return heuristic;
    }

    private static bool PositionIsDestination(GridTile gridTileOrigin, GridTile gridTileDestination)
    {
        return gridTileOrigin.tuplePosition == gridTileDestination.tuplePosition;
    }

    private GridTile GetGridTupleByPosition(Vector2 position)
    {
        foreach (var tuple in _gridTuples)
        {
            if (tuple.tuplePosition.Equals(position))
            {
                return tuple;
            }
        }

        return new GridTile();
    }
}