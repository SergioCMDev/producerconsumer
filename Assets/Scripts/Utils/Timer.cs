﻿using System;
using System.Collections;
using UnityEngine;

namespace Utils
{
    public class Timer
    {
        public event Action OnTimerEnds = delegate { };
        private float _timeToWait;

        public void TimeToWait(float time)
        {
            _timeToWait = time;
        }


        public IEnumerator TimerCoroutine()
        {
            yield return new WaitForSeconds(_timeToWait);
            OnTimerEnds.Invoke();
        }
    }
}