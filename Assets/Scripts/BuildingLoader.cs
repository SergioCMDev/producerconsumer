﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingLoader : MonoBehaviour
{
    [SerializeField] private int _producersToCreateAtFirst,
        _consumersToCreateAtFirst,
        _vehiclesToCreateAtFirst,
        _distanceToBeNear;

    [SerializeField] private GameObject _consumerPrefab, _producerPrefab, _vehiclePrefab, _roadPrefab;

    private List<GameObject> _consumersCreatedInstances, _producersCreatedInstances, _vehiclesCreatedInstances;

    private List<Consumer> _consumersCreated = new List<Consumer>();
    private List<Producer> _producersCreated = new List<Producer>();
    private List<GridTile> _gridTuples;

    private VehicleScheduler _vehicleScheduler;
    private Astar _astar;

    private void Awake()
    {
        _vehicleScheduler = new VehicleScheduler();
    }

    private void OnDestroy()
    {
        _vehicleScheduler?.Destroy();
    }

    private List<GameObject> InstantiateItems(int quantityToCreate, GameObject prefabToCreate, string instanceName)
    {
        var listOfInstantiatedItems = new List<GameObject>();
        for (int i = 0; i < quantityToCreate; i++)
        {
            var producerInstantiated = Instantiate(prefabToCreate, transform);
            producerInstantiated.gameObject.SetActive(false);
            producerInstantiated.name = $"{instanceName} {i}";
            listOfInstantiatedItems.Add(producerInstantiated);
        }

        return listOfInstantiatedItems;
    }

    public void Initialize()
    {
        _consumersCreatedInstances = InstantiateItems(_consumersToCreateAtFirst, _consumerPrefab, "Consumer");
        _producersCreatedInstances = InstantiateItems(_producersToCreateAtFirst, _producerPrefab, "Producer");

        foreach (var consumerInstance in _consumersCreatedInstances)
        {
            var consumer = consumerInstance.GetComponent<Consumer>();
            _consumersCreated.Add(consumer);
            _vehiclesCreatedInstances =
                InstantiateItems(_vehiclesToCreateAtFirst, _vehiclePrefab, $"{consumer.name} Vehicle");
            consumer.SetInstantiatedVehicles(_vehiclesCreatedInstances);
        }

        foreach (var producerInstance in _producersCreatedInstances)
        {
            var producer = producerInstance.GetComponent<Producer>();
            _producersCreated.Add(producer);
        }

        _vehicleScheduler.Initialize(_producersCreated, _consumersCreated);
    }


    public GameObject GenerateConsumerAtPosition(Vector2 position)
    {
        var consumer = GetNewConsumer();
        if (!consumer) return null;
        try
        {
            var gridTile = _gridTuples.Single(x =>
                x.tuplePosition.x.Equals(position.x) && x.tuplePosition.y.Equals(position.y));

            consumer.gameObject.SetActive(true);
            consumer.transform.position = position;
            gridTile.tileEntity = consumer;
            gridTile.tileEntity.Walkable = false;
            consumer.Initialize();
            return consumer.gameObject;
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while creating new consumer {e}");
        }

        return null;
    }

    private Consumer GetNewConsumer()
    {
        foreach (var instance in _consumersCreated)
        {
            if (instance.gameObject.activeInHierarchy) continue;
            return instance;
        }

        return null;
    }

    private Producer GetNewProducer()
    {
        foreach (var instance in _producersCreated)
        {
            if (instance.gameObject.activeInHierarchy) continue;
            return instance;
        }

        return null;
    }

    public void SetGrid(List<GridTile> getGrid)
    {
        _gridTuples = getGrid;
    }

    public GameObject GenerateProducerAtPosition(Vector2 position)
    {
        var producer = GetNewProducer();
        if (!producer) return null;
        try
        {
            var tuple = _gridTuples.Single(x =>
                x.tuplePosition.x.Equals(position.x) && x.tuplePosition.y.Equals(position.y));

            producer.transform.position = position;
            producer.gameObject.SetActive(true);
            tuple.tileEntity = producer;
            tuple.tileEntity.Walkable = false;

            producer.Initialize();
            return producer.gameObject;
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while creating new producer {e}");
        }

        return null;
    }

    public void GeneratePathfindingBetweenConsumersAndProducers(List<GameObject> consumerInstances,
        List<GameObject> producersInstances)
    {
        foreach (var consumerInstance in consumerInstances)
        {
            foreach (var producerInstance in producersInstances)
            {
                if (BuildingsAreAway(consumerInstance, producerInstance) || BuildingsAreTooClose(consumerInstance, producerInstance)) continue;

                var consumer = consumerInstance.GetComponent<Consumer>();
                var producer = producerInstance.GetComponent<Producer>();
                consumer.Walkable = false;
                producer.Walkable = false;
                var pathConsumerToProducer = _astar.GetPath(consumerInstance.transform.position,
                    producerInstance.transform.position);
                consumer.AddPathToProducer(producer, pathConsumerToProducer);
                GenerateRoadsInPath(pathConsumerToProducer);
            }
        }
    }

    private bool BuildingsAreTooClose(GameObject consumerInstance, GameObject producerInstance)
    {
        var distanceX = Mathf.Abs(consumerInstance.transform.position.x - producerInstance.transform.position.x);
        var distanceY = Mathf.Abs(consumerInstance.transform.position.y - producerInstance.transform.position.y);
        return distanceX < 1 || distanceY < 1;
    }

    private void GenerateRoadsInPath(List<GridTile> pathConsumerToProducer)
    {
        for (var index = 1; index < pathConsumerToProducer.Count - 1; index++)
        {
            var roadInstance = Instantiate(_roadPrefab, transform);
            var pathTile = pathConsumerToProducer[index];
            var road = roadInstance.GetComponent<RoadTile>();

            // var road = roadInstance.GetComponent<RoadTile>();
            road.transform.position = pathTile.tuplePosition;
            pathTile.tileEntity = road;
            road.Initialize();
        }
    }

    private bool BuildingsAreAway(GameObject consumerInstance, GameObject producerInstance)
    {
        return UtilsDistance.GetDistanceBetweenTwoPoints(consumerInstance.transform.position,
            producerInstance.transform.position) > _distanceToBeNear;
    }

    public void SetPathfinding(Astar astar)
    {
        _astar = astar;
    }
}